const babelJest = require('babel-jest');
const paths = require('../paths');

module.exports = babelJest.createTransformer(paths.babelConfig);