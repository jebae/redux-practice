const paths = require('../paths');

module.exports = {
	verbose: true,
	rootDir: paths.rootDir,
	moduleFileExtensions: [
    'ts', 'js', 'tsx', 'jsx'
  ],
	modulePaths: [
		"src"
	],
	testRegex: '(/test/.*|\\.(test|spec))\\.(ts|tsx|js)$',
	setupTestFrameworkScriptFile: '<rootDir>/configs/jest/enzymeConfig.js',
	setupFiles: ['raf/polyfill'],
	transform: {
		'^.+\\.tsx?$': '<rootDir>/node_modules/ts-jest/preprocessor.js',
		'^.+\\.jsx?$': '<rootDir>/configs/jest/fileTransform.js',
		'^.+\\.scss?$': '<rootDir>/configs/jest/cssTransform.js'
	},
	globals: {
		"ts-jest": {
			tsConfigFile: 'configs/tsconfig.json',
			babelConfig: paths.babelConfig
		}
	}
}