module.exports = {
	babelConfig: require('./babelrc'),
	tsConfig: require('./tsconfig'),
	rootDir: process.cwd()
}