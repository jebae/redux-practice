module.exports = {
	presets: ["es2015", "react"],
	sourceMaps: true,
	plugins: [
		"transform-class-properties",
		"transform-object-rest-spread",
		"transform-runtime",
		"transform-async-to-generator"
	]
}