const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader')
const paths = require('./paths');
const path = require('path');

module.exports = {
	entry: [
		paths.rootDir + '/src/index.js',
		'webpack-dev-server/client?http://localhost:3000',
		'webpack/hot/only-dev-server'
	],
	output: {
		path: '/',
		publicPath: '/',
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: './public',
		port: 3000,
		historyApiFallback: true
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.jsx']
	},
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: 'babel-loader',
				exclude: [
					paths.rootDir + '/node_modules/'
				],
				query: Object.assign({ cacheDirectory: true }, paths.babelConfig)
			},
			{
				test: /.tsx?$/,
				exclude: [
					paths.rootDir + '/node_modules/'
				],
				use: [
					{
						loader: 'babel-loader',
						query: Object.assign({ cacheDirectory: true }, paths.babelConfig)
					},
					{
						loader: 'awesome-typescript-loader',
						query: {
							configFileName: 'configs/tsconfig.json'
						}
					}
				]
			},
			{
				test: /.scss$/,
				exclude: paths.rootDir + '/node_modules/',
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				]
			}
		]
	},
	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		// new CheckerPlugin(),
		new HtmlWebpackPlugin({
			template: './public/index.html'
		})
	]
}