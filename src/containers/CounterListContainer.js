import React from 'react';
import { connect } from 'react-redux';
import CounterList from '../components/CounterList';
import * as CounterActions from '../modules/Counter';
import * as PostActions from '../modules/Post';
import { getRandomColor } from '../utils';

const mapStateToProps = (state) => ({
	counters: state.CounterList.counters
})

const mapDispatchToProps = (dispatch) => {
	return {
		onIncrement: (index) => dispatch(CounterActions.increment(index)),
		onDecrement: (index) => dispatch(CounterActions.decrement(index)),
		onSetColor: (index) => {
			const color = getRandomColor();
			dispatch(CounterActions.setColor({ index, color }))
		},
		onGetPost: (postId) => dispatch(PostActions.getPost(postId))
	}	
}

export default connect(
	mapStateToProps,
	mapDispatchToProps	
)(CounterList);