import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import Reducers from './modules';
import { Provider } from 'react-redux';
import App from './app';
import { thunkMiddleware } from './lib/middleware';

const middlewares = applyMiddleware(
	thunkMiddleware
);

const store = createStore(Reducers, middlewares);

ReactDOM.render(
	<Provider store={ store }>
    <App/>
	</Provider>,
  document.getElementById('root')
)