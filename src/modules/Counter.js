// action types
const CREATE = 'redux-prac/counter/CREATE';
const REMOVE = 'redux-prac/counter/REMOVE';
const INCREMENT = 'redux-prac/counter/INCREMENT';
const DECREMENT = 'redux-prac/counter/DECREMENT';
const SET_COLOR = 'redux-prac/counter/SET_COLOR';

// initial state
const initialState = {
	counters: [
		{
			color: 'black',
			number: 0
		}
	]
}

// reducer
export default (state=initialState, action) => {
	const { counters } = state;
	switch (action.type){

		case CREATE:
			return {
				counters: [
					...counters, {
						color: 'black',
						number: 0
					}
				]
			}

		case REMOVE:
			return {
				counters: counters.slice(0, counters.length - 1)
			}

		case INCREMENT:
			return {
				counters: [
					...counters.slice(0, action.index),
					{
						...counters[action.index],
						number: counters[action.index].number + 1
					},
					...counters.slice(action.index + 1, counters.length)
				]
			}

		case DECREMENT:
			return {
				counters: [
					...counters.slice(0, action.index),
					{
						...counters[action.index],
						number: counters[action.index].number - 1
					},
					...counters.slice(action.index + 1, counters.length)
				]
			}

		case SET_COLOR:
			return {
				counters: [
					...counters.slice(0, action.index),
					{
						...counters[action.index],
						color: action.color
					},
					...counters.slice(action.index + 1, counters.length)
				]
			}

		default:
			return state;
	}
}

// action creators
export const create = () => ({
	type: CREATE
})

export const remove = () => ({
	type: REMOVE
})

export const increment = (index) => ({
	type: INCREMENT,
	index
})

export const asyncIncrement = index => dispatch => {
	setTimeout(
		() => dispatch(increment(index)),
		1000
	)
}

export const decrement = (index) => ({
	type: DECREMENT,
	index
})

export const setColor = ({ index, color }) => ({
	type: SET_COLOR,
	index,
	color
})