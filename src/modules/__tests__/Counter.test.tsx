import reducer, * as actions from '../../modules/Counter';

const initialState = {
	counters: [
		{
			color: 'black',
			number: 0
		}
	]
}

describe('Counter Reducer', () => {
	it('should have initial store', () => {
		expect(
			reducer(undefined, { type: undefined })
		).toEqual(initialState);
	})

	it('should create one counter', () => {
		const action = actions.create();
		const createdCounter = { color: 'black', number: 0 };
		
		expect(
			reducer(initialState, {...action})
		).toEqual({
			counters: [...initialState.counters, createdCounter]
		})
	})

	it('should remove one counter', () => {
		const action = actions.remove();
		const existedState = {
			counters: [...initialState.counters, { color: 'black', number: 0 }]
		};

		expect(
			reducer(existedState, {...action})
		).toEqual(initialState)
	})

	it('should increment number', () => {
		const index = 1;
		const action = actions.increment(index);
		const existedState = {
			counters: [
				...initialState.counters,
				{ color: 'black', number: 0 },
				{ color: 'black', number: 0 }
			]
		};
		const counters = existedState.counters;

		expect(
			reducer(existedState, {...action})
		).toEqual({
			counters: [
				...counters.slice(0, index),
				{ color: 'black', number: 1 },
				...counters.slice(index+1, counters.length)
			]
		})
	})
})