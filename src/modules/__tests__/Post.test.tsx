jest.mock('services/Post');
import configureMockStore from 'redux-mock-store';
import { thunkMiddleware } from '../../lib/middleware';
import * as actions from '../../modules/Post';
import getPostAPI from '../../services/Post';

let middlewares, mockStore, store;

beforeEach(() => {
	middlewares = [ thunkMiddleware ];
	mockStore = configureMockStore(middlewares);
	store = mockStore({});
})

describe('Post', () => {
	it('should get success post', async () => {
		const expectedData = { data: { title: 'test title', body: 'test body' } };
		getPostAPI.mockReturnValue(
			new Promise((resolve, reject) => {
				resolve(expectedData);
			})
		);
		await store.dispatch(actions.getPost(1));
		expect(store.getActions()).toEqual([
			{ type: actions.GET_POST_PENDING },
			{ type: actions.GET_POST_SUCCESS , ...expectedData}
		])
	})

	it('should get failure post', async () => {
		getPostAPI.mockReturnValue(
			new Promise((resolve, reject) => {
				reject();
			})
		)
		await store.dispatch(actions.getPost(1));
		expect(store.getActions()).toEqual([
			{ type: actions.GET_POST_PENDING },
			{ type: actions.GET_POST_FAILURE }
		])
	})
})