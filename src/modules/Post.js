import getPostAPI from '../services/Post.js';

// action types
export const GET_POST_PENDING = 'redux-prac/post/GET_PENDING';
export const GET_POST_SUCCESS = 'redux-prac/post/GET_SUCCESS';
export const GET_POST_FAILURE = 'redux-prac/post/GET_FAILURE';

// initial state
const initialState = {
	pending: false,
	error: false,
	data: {
		title: '',
		body: ''
	}
}

// reducer
export default (state=initialState, action) => {
	switch(action.type){

		case GET_POST_PENDING:
			return {
				...state,
				pending: true,
				error: false,
			};

		case GET_POST_SUCCESS:
			const { title, body } = action.data;
			return {
				...state,
				pending: false,
				data: {
					title, 
					body
				}
			}

		case GET_POST_FAILURE:
			return {
				...state,
				pending: false,
				error: true
			}

		default:
			return state;
	}
}

// action creators
export const getPost = postId => dispatch => {
	dispatch({ type: GET_POST_PENDING });
	
	return getPostAPI(postId).then(
		response => {
			dispatch({
				type: GET_POST_SUCCESS,
				data: response.data
			});
		}
	).catch(
		e => {
			dispatch({ type: GET_POST_FAILURE });
		}
	)
}