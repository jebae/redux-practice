import { combineReducers } from 'redux';
import CounterList from './Counter';
import Post from './Post';

export default combineReducers({
	CounterList,
	Post
});