import React from 'react';
import { connect } from 'react-redux';
import CounterList from './containers/CounterListContainer';
import Buttons from './components/Buttons';
import Post from './components/Post';
import * as CounterActions from './modules/Counter';
import * as PostActions from './modules/Post';

class App extends React.Component{
	componentDidMount(){
		const { onLoadPost } = this.props;
		const firstCircle = this.props.firstCircle;
		if (firstCircle){
			const postId = firstCircle.number;
			onLoadPost(postId);
		}
	};

  render(){
		const { onCreate, onRemove, post, loading, error } = this.props;
    return (
			<div>
				<Buttons 
					onCreate={ onCreate }
					onRemove={ onRemove }
				/>
				<CounterList/>
				{ loading && 'Loading...' }
				{ error
					? 'Quack Error'
					: <Post title={ post.title } body={ post.body }/>}
			</div>
    )
  }
}

const mapStateToProps = (state) => {
	const { data, pending, error } = state.Post;
	const firstCircle = state.CounterList.counters[0];

	return {
		firstCircle,
		post: data,
		loading: pending,
		error
	};
};

const mapDispatchToProps = (dispatch) => ({
	onCreate: () => dispatch(CounterActions.create()),
	onRemove: () => dispatch(CounterActions.remove()),
	onLoadPost: (postId) => dispatch(PostActions.getPost(postId))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);