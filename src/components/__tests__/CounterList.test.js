import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import CounterList from '../CounterList';

let CounterListComponent, mockCounters, clickFn;

beforeEach(() => {
	clickFn = jest.fn();
	mockCounters = [
		{number: 1, color: 'yellow'},
		{number: 1, color: 'red'},
		{number: 1, color: 'pink'}
	];
	CounterListComponent = mount(
		<CounterList 
			counters={ mockCounters }
			onIncrement={ clickFn }
			/>
	);
})

describe.skip('CounterList', () => {
	it('should mount', () => {
		mount(<CounterList/>);
	})

	describe('Click', () => {
		it('should react to click', () => {
			const firstCounter = CounterListComponent.find('Counter').first();
			expect(clickFn.mock.calls.length).toEqual(0);
			firstCounter.simulate('click');
			expect(clickFn.mock.calls.length).toEqual(1);
		})
	})
})

afterEach(() => {
  clickFn.mockReset();
});