import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import Counter from '../Counter';

let CounterComponent, clickFn;

beforeEach(() => {
	clickFn = jest.fn();
	CounterComponent = mount(
		<Counter 
			number={1} 
			color='black'
			onIncrement={ clickFn }/>
	);
})

describe.skip('Counter', () => {
	it('should mount', () => {
		mount(<Counter/>);
	})

	describe('render', () => {
		it('should render html like this', () => {
			const dom = `<div class="Counter" style="background-color: black;">1</div>`;
			expect(CounterComponent.html()).toEqual(dom);
		})
	})

	describe('props', () => {
		it('should have props color and number', () => {
			const colorExptected = 'black';
			const numberExptected = 1;
			expect(CounterComponent.instance().props.color).toEqual(colorExptected);
			expect(CounterComponent.instance().props.number).toEqual(numberExptected);
		})
	})

	describe('click', () => {
		it('should react to click', () => {
			expect(clickFn.mock.calls.length).toEqual(0);
			CounterComponent.simulate('click');
			expect(clickFn.mock.calls.length).toEqual(1);
		})
	})
})