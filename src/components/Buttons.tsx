import * as React from 'react';
import '../stylesheets/components/Button.scss';

interface ButtonProps {
	onCreate: () => void,
	onRemove: () => void
}

const Buttons: React.SFC<ButtonProps> = ({ onCreate, onRemove }) => {
	return (
		<div className="Buttons">
			<div className="btn add" onClick={ onCreate }>Create</div>
			<div className="btn remove" onClick={ onRemove }>Remove</div>
		</div>
	)
}

Buttons.defaultProps = {
	onCreate: () => console.warn('onCreate is not defined'),
	onRemove: () => console.warn('onRemove is not defined')
}

export default Buttons;