import * as React from 'react';
import * as PostActions from '../modules/Post';
import '../stylesheets/components/Counter.scss';

interface CounterProps {
	index: number,
	color: string,
	number: number,
	onIncrement: (index: number) => void,
	onDecrement: (index: number) => void,
	onSetColor: (index: number) => void,
	onGetPost: (postId: number) => void
};

class Counter extends React.Component<CounterProps>{
	async getPost(postId){
		const { onGetPost } = this.props;
		try {
			await onGetPost(postId);
			console.log('after load post');
		} catch(e) {
			console.log(e);
		};
	}

	componentWillReceiveProps(nextProps){
		const { number } = this.props;
		if (number != nextProps.number){
			this.getPost(nextProps.number);
		}
	}

	render(){
		const { index, color, number, onIncrement, onDecrement, onSetColor, onGetPost } = this.props;
		const onClick = () => onIncrement(index);
		const onContextMenu = (e) => {
			e.preventDefault();
			onDecrement(index);
		};
		const onDoubleClick = () => onSetColor(index);

		return (
			<div 
				className="Counter"
				onClick={ onClick }
				onContextMenu={ onContextMenu }
				onDoubleClick={ onDoubleClick }
				style={ {backgroundColor: color} }
			>
				{ number }
			</div>
		);
	}
}

// prop types


// Counter.defaultProps = {
// 	color: 'black',
// 	number: 0,
// 	onIncrement: () => {
// 		console.log('hello');
// 		console.warn('onIncrement not defined')
// 	},
// 	onDecrement: () => console.warn('onDecrement not defined'),
// 	onSetColor: () => console.warn('onSetColor not defined'),
// 	onGetPost: () => console.warn('onGetPost not defined')
// };

export default Counter;