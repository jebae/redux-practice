import * as React from 'react';
import '../stylesheets/components/Post.scss';

interface PostProps {
	title: string,
	body: string
}

const Post: React.SFC<PostProps> = ({ title, body }) => {
	return (
		<div className="Post">
			<h2>{ title }</h2>
			<p>{ body }</p>
		</div>
	)
}

Post.defaultProps = {
	title: '',
	body: ''
}

export default Post;