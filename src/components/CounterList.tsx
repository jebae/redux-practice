import * as React from 'react';
import Counter from './Counter';

interface CounterListProps {
	counters: Array<any>,
	onIncrement: () => void,
	onDecrement: () => void,
	onSetColor: () => void,
	onGetPost: () => void
}

const CounterList: React.SFC<CounterListProps> = ({ 
	counters,
	onIncrement,
	onDecrement,
	onSetColor,
	onGetPost 
}) => {
	const counterList = counters.map((counter, i) => (
		<Counter
			key={ i }
			index={ i }
			{ ...counter }
			onIncrement={ onIncrement }
			onDecrement={ onDecrement }
			onSetColor={ onSetColor }
			onGetPost={ onGetPost }
		/>
	));

	return (
		<div>
			{ counterList }
		</div>
	)
}

CounterList.defaultProps = {
	counters: [],
	onIncrement: () => console.warn("No increment function"),
	onDecrement: () => console.warn("No decrement function"),
	onSetColor: () => console.warn("No setColor function"),
	onGetPost: () => console.warn("No getpost function")
}

export default CounterList;